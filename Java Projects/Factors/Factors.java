import java.util.*;

public class Factors
{
    public static void main(String []argv)
    {
    
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter The Number ");
        int number = sc.nextInt();
        
        System.out.println("Factors Of "+number+" Are: ");
        for (int i = 1; i <= number; ++i)
        {
            if (number % i == 0)
            {
                System.out.println(i + " ");
            }
        }
    }
    
}