import java.util.*;

public class Swap
{
    public static void main(String []argv)
    {
        int temp;
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number 1");
        int n1 = sc.nextInt();
        System.out.println("Enter Number 2");
        int n2 = sc.nextInt();    

        System.out.println("Before Swapping");
        System.out.println("Number 1 = "+n1);
        System.out.println("Number 2 = "+n2);

        temp = n1;
        n1 = n2;
        n2 = temp;

        System.out.println("After Swapping");
        System.out.println("Number 1 = "+n1);
        System.out.println("Number 2 = "+n2);
    }
}