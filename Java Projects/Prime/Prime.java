import java.util.*;

public class Prime
{
    public static void main(String []argv)
    {
        int i = 2;
        int flag = 0;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter The Number You Want To Check");
        int num = sc.nextInt();

        while (i <= num / 2)
        {
        
            if (num % i == 0)
            {
                flag = 1;
                break;
            }
            ++i;
        }
  
      if (flag == 0)
        System.out.println(num + " is a prime number.");
      else
        System.out.println(num + " is not a prime number.");
    }
  }