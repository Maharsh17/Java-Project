import java.util.Scanner;

public class HCFLCM
{
   public static void main(String []argv)
   {
      int temp1, temp2,temp, hcf, lcm;
      Scanner sc = new Scanner(System.in);

      System.out.print("Enter First Number: ");
      int num1 = sc.nextInt();
      System.out.print("Enter Second Number: ");
      int num2 = sc.nextInt();

      temp1 = num1;
      temp2 = num2;

      while(temp2 != 0)
      {
         temp = temp2;
         temp2 = temp1%temp2;
         temp1 = temp;
      }

      hcf = temp1;
      lcm = (num1*num2)/hcf;

      System.out.println("HCF Is "+hcf);
      System.out.println("LCM Is "+lcm);
   }
}
