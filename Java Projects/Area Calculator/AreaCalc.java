import java.util.*;

class AreaCalc
{
    public static void main(String args[])
	{
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter 1 For Square");
        System.out.println("Enter 2 For Rectangle");
        System.out.println("Enter 3 For Circle");
        System.out.println("Enter 4 For Triangle");
        System.out.println("Enter 5 For Pentagon");
        System.out.println("Enter 6 For Hexagon");

        int ch = sc.nextInt();

        switch(ch)
        {
            case 1:
                System.out.println("Enter The Side Of The Square");
                int x = sc.nextInt();
                System.out.println("The Area Of The Square Is "+Math.pow(x, 2));
            break;

            case 2:
                System.out.println("Enter Lenght Of The Reactangle");
                int a = sc.nextInt();
                System.out.println("Enter Breadth Of The Reactangle");
                int b = sc.nextInt();
                System.out.println("The Area Of The Rectangle Is "+a*b);
            break;

            case 3:
                System.out.println("Enter The Radius Of The Circle");
                int r = sc.nextInt();
                double z = 3.14 * r * r;
                System.out.println("the area of the circle is "+z+" sq units");
            break;

            case 4:
                System.out.println("Enter Height Of The Triangle");
                int ab = sc.nextInt();
                System.out.println("Enter Base Of The Triangle");
                int bc = sc.nextInt();

                int cv = (ab*bc)/2;
                System.out.println("The Area For Traingle Is "+cv);
            break;

            case 5:
                System.out.println("Enter Side Of Pentagon");
                int s = sc.nextInt();
                System.out.println("Enter Radius Of Pentagon");
                int b = sc.nextInt();

                float are=(5.0/2.0)*s*b;
                System.out.println("The Area Is "+are);
            break;

            case 6:
                System.out.println("Enter Side Of The Hexagon");
                int sq = sc.nextInt();
                float cm = (6*(sq*sq))/(4*Math.tan(Math.PI/6));

                System.out.println("The Area For Hexagon Is "+cm);

            break;
        }
    }
}
