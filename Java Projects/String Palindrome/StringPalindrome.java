import java.util.*;

public class StringPalindrome
{
   public static void main (String []argv)
   {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter The String You Want To Check");
      String str = sc.nextLine();

      String reverse = new StringBuffer(str).reverse().toString();
      if (str.equals(reverse))
      System.out.println("String Is Palindrome");
      else
      System.out.println("String Is Not Palindrome");
   }
}
