import java.util.*;

public class UpperAndLower
{
   public static void main(String []argv)
   {
      Scanner sc = new Scanner(System.in);

      System.out.println("Enter The String To Convert From Upper To Lower");
      String str1 = sc.nextLine();
      System.out.println("The String In LowerCase Is :-" + str1.toLowerCase());

      System.out.println("Enter The String To Convert From Lower To Upper");
      String str2 = sc.nextLine();
      System.out.println("The String In UpperCase Is :-" + str2.toUpperCase());

   }
}
