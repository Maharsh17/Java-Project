import java.util.*;

class Armstrong
{  
    public static void main (String []argv)  
    {    
        int c=0,a;    

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter The Number You Want To Check");
        int n = sc.nextInt();

        int result=n;

        while(n>0)  
        {  
            a=n%10;  
            n=n/10;  
            c=c+(a*a*a);  
        }  
        
        if(result==c)
        {  
            System.out.println(result+" Is Armstrong Number");   
        }
        
        else
        {  
          System.out.println(result+" Is Not Armstrong Number");   
        }         
    }
}  