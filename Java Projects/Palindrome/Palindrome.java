import java.util.*;
public class Palindrome
{
	public static void main(String []argv)
	{
		int sum=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter A Number To Check");

		int i = sc.nextInt();
		int temp=i;

		while(i>0)
		{
				int r=i%10;
		    sum=(sum*10)+r;
		    i=i/10;
		}

		if(temp==sum)
		{
				System.out.println("The Number Is Palindrome");
		}
		else
		{
		   	System.out.println("The Number Is Not Palindrome");
		}

	}
}
