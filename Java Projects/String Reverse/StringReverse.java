import java.util.*;

public class StringReverse
{
    public static void main(String []argv)
    {
      Scanner sc = new Scanner(System.in);

      System.out.println("Enter The String You Want To Replace");
      String str = sc.nextLine();
      StringBuilder sb = new StringBuilder(str);

      System.out.println("The Reverse String Is "+sb.reverse());
    }
}
