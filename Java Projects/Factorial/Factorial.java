import java.util.*;

public class Factorial
{
    public static void main(String []argv) 
    {
        long factorial = 1;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter The Number");
        int num = sc.nextInt();

        for(int i = 1; i <= num; ++i)
        {
            factorial = factorial*i;
            System.out.println("Factorial Of "+num+" At Position "+i+" = "+factorial);
        }
        
    }
}
