import java.util.*;

public class CISI
{

    public static void main(String []argv)
    {

      Scanner sc = new Scanner(System.in);

      System.out.println("Enter Principle");
      int p = sc.nextInt();
      System.out.println("Enter Time");
      int t = sc.nextInt();
      System.out.println("Enter Rate In Decimal");
      float r = sc.nextInt();
      System.out.println("Enter How Many Time Is It Compounded");
      int n = sc.nextInt();


      double amount = p * Math.pow(1 + (r / n), n * t);
      double cinterest = amount - p;
      System.out.println("Compound Interest After " + t + " Years Is "+cinterest);
      System.out.println("Amount After " + t + " Years Is "+amount);


    }
}
