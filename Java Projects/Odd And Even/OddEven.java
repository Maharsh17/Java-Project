import java.util.*;

public class OddEven{

    public static void main(String []argv)
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter The Number");
        int number = sc.nextInt();

        if(number/2 == 0)
        {
            System.out.println("The Number Is A Even Number");
        }
        else
        {
            System.out.println("The Number Is A Odd Number");
        }
    }

}